<?php
/**
 * @file
 * Wizard implementation for Commons migrations.
 */

/*
 * Implementation of hook_migrate_api().
 */
function commons_migration_wizard_migrate_api() {
  $api = array(
    'api' => 2,
    'wizard classes' => array('MigrateCommonsWizard'),
  );
  return $api;
}


class MigrateCommonsWizard extends MigrateD2DWizard {

  /**
   * Lay out the basic steps of the wizard.
   */
  public function __construct() {
    parent::__construct();

    // $this->addStep(t('Credentials'), 'sourceDataForm');
  }

    /**
   * Identify ourselves.
   *
   * @return string
   */
  public function getSourceName() {
    return t('Commons 2.x');
  }

}
