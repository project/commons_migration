<?php

/**
 * @file Question migration classes for Commons
 */

class CommonsPollMigration extends CommonsNodeMigration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);
    $this->description = t('Migration of Commons 2.x "Poll" content');

    $this->addFieldMapping('active')
      ->defaultValue(1);

    $this->addFieldMapping('runtime', 'runtime')
      ->defaultValue(0);
    $this->addFieldMapping('choice', 'choice')
      ->description('src_choices populated in prepareRow()');

    $this->addFieldMapping('votes', 'votes')
      ->description('src_votes populated in prepareRow()');

    $this->addFieldMapping('field_topics', 2)
      ->sourceMigration('CommonsTaxonomyTags');
    $this->addFieldMapping('field_topics:source_type')
      ->defaultValue('tid');


    $this->highwaterField = array(
      'name' => 'changed',
      'type' => 'int',
    );

    $this->addUnmigratedSources(
      array(
        'log',
      )
    );


    $this->addUnmigratedDestinations(
      array(
        'og_group_ref',
        'field_radioactivity',
        'field_topics:create_term',
        'field_topics:ignore_case',
      )
    );
  }

  public function prepareRow($row) {
    print_r($row);
    $choices =  Database::getConnection('default', 'legacy')
      ->select('poll_choices', 'c')
      ->fields('c', array('chtext', 'chorder', 'chvotes', 'nid'))
      ->condition('c.nid', $row->nid)
      ->execute();
    $row->choice = array();
    foreach ($choices as $choice) {
      $row->choice[] = array(
        'chtext' => $choice->chtext,
        'chvotes' => $choice->chvotes,
        'weight' => $choice->chorder,
      );
    }
    // Note that we won't know until much later what the chid is for each
    // choice, so it's best to tie the votes to choices by text.
    $query =  Database::getConnection('default', 'legacy')
      ->select('poll_votes', 'v')
      ->fields('v', array('uid', 'hostname', 'nid'))
      ->condition('v.nid', $row->nid);
    $query->innerJoin('poll_choices', 'c', 'v.nid=c.nid AND v.chorder=c.chorder');
    $query->fields('c', array('chtext'));
    $votes = $query->execute();
    $row->votes = array();
    foreach ($votes as $vote) {
      $row->votes[] = array(
        'chtext' => $vote->chtext,
        'uid' => $vote->uid,
        'hostname' => $vote->hostname,
        'timestamp' => REQUEST_TIME,
      );
    }
    return TRUE;
  }
}
