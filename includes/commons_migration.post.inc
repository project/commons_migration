<?php

/**
 * @file Post migration classes for Commons
 */

class CommonsPostMigration extends CommonsNodeMigration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);
    $this->description = t('Migration of Commons 2.x "Post" content');



    //TODO: Set a highwater field

    $this->addUnmigratedSources(
      array(
        'log',
      )
    );

    $this->addUnmigratedDestinations(
      array(
        'og_group_ref',
        'field_radioactivity',
        'field_topics',
        'field_topics:source_type',
        'field_topics:create_term',
        'field_topics:ignore_case',
      )
    );
  }
}
