<?php

/**
 * @file Question migration classes for Commons
 */

class CommonsQuestionMigration extends CommonsNodeMigration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);
    $this->description = t('Migration of Commons 2.x "Question" content');

    //TODO: Set a highwater field

    $this->addUnmigratedSources(
      array(
        'log',
        'field_answer_count',
        'field_notify_p',
        'field_question_locked_p',
      )
    );

    $this->addUnmigratedDestinations(
      array(
        'og_group_ref',
        'field_radioactivity',
      )
    );
  }
}
