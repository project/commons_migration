<?php

/**
 * @file Wiki migration classes for Commons
 */

class CommonsWikiMigration extends CommonsNodeMigration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);
    $this->description = t('Migration of Commons 2.x "Wiki" content');



    //TODO: Set a highwater field

    $this->addFieldMapping('field_topics', 1)
      ->sourceMigration('CommonsTaxonomyTags');
    $this->addFieldMapping('field_topics:source_type')
      ->defaultValue('tid');

    $this->addUnmigratedSources(
      array(
        'log',
      )
    );

    $this->addUnmigratedDestinations(
      array(
        'og_group_ref',
        'field_radioactivity',
        'field_topics:create_term',
        'field_topics:ignore_case',
      )
    );
  }
}
