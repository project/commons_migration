<?php

class CommonsNodeMigration extends DrupalNode6Migration {
  public function __construct(array $arguments) {

    parent::__construct($arguments);

    //$this->addFieldMapping('is_new', NULL, FALSE)->defaultValue(TRUE);
    //$this->addFieldMapping('nid', 'nid');

    // Use a Commons-specific format ID to machine name mapping.
    $this->addFieldMapping('body:format', 'format', FALSE)
      ->callbacks(array($this, 'commonsBodyFormat'));

    $this->addUnmigratedSources(
      array(
        'revision',
        'revision_uid',
      )
    );

    if (module_exists('statistics')) {
      $this->addUnmigratedSources(
        array(
          'totalcount',
          'daycount',
          'timestamp',
        )
      );
    }
  }

  /**
   * Copy of mapFormat() for mapping to machine names as appropriate for Commons
   * instead of to their direct equivalents.
   *
   * @see mapFormat()
   */
  protected function commonsBodyFormat($format) {
    if (!is_array($format)) {
      $format = array($format);
    }
    $result = array();
    foreach ($format as $format_value) {
      if (isset($format_value)) {
        switch($format_value) {
          case 1:
            $result[] = 'filtered_html';
            break;
          case 2:
            $result[] = 'full_html';
            break;
          default:
            // Ignore other text formats and default to filtered HTML.
            $result[] = 'filtered_html';
        }
      }
      else {
        $result[] = NULL;
      }
    }
    // Only return an array if we have to.
    if (count($result) > 1) {
      return $result;
    }
    else {
      return reset($result);
    }
  }
}
