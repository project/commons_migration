<?php

/**
 * @file
 * Create user relation to group.
 */

class CommonsMigrationOgMigrateUser extends OgMigrateUser {

  /**
   * Public constructor
   *
   * @param array $arguments Optional array of arguments to use in migration and
   *   pass up to Migration parent class (version 2.6 and later)
   */
  public function __construct(array $arguments) {

    parent::__construct($arguments);

    $connection_array = array(
      'source_connection' => $arguments['source_connection'],
      'source_version' => $arguments['source_version'],
      'group_name' => $arguments['group_name'],
    );

    //Hacky?
    $this->dependencies = array();

    foreach (node_type_get_names() as $bundle => $value) {
      // Dependent on a dynamic migration.
      $machine_name = 'CommonsMigrationOgMigrateGroup' . ucfirst($bundle);
      $dependency_arguments = $connection_array + array(
          'bundle' => $bundle,
        );
      if (MigrationBase::getInstance($machine_name, 'CommonsMigrationOgMigrateGroup', $dependency_arguments)) {
        $this->dependencies[] = $machine_name;
      }
    }

    $this->addFieldMapping('gid', 'nid')->sourceMigration(array('CommonsGroup'));

  }

}
