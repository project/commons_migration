<?php

/**
 * @file Base class for commons migration
 */

abstract class CommonsMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    // Database connections are configured in settings.php
    $this->connection = Database::getConnection('default', 'legacy');

  }
}
