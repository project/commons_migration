<?php

/**
 * @file Event migration classes for Commons
 */

class CommonsEventMigration extends CommonsNodeMigration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);
    $this->description = t('Migration of Commons 2.x "Event" content');

    //TODO: Set a highwater field

    $this->addFieldMapping('field_address')->defaultValue('US');
    $this->addFieldMapping('field_address:thoroughfare', 'field_location');


    $this->addFieldMapping('sticky', 'field_featured_content')
      ->callbacks(array($this, 'translateFeatured'));

    $this->addFieldMapping('field_date', 'field_date');
    $this->addFieldMapping('field_topics', 1)
      ->sourceMigration('CommonsTaxonomyTags');
    $this->addFieldMapping('field_topics:source_type')
      ->defaultValue('tid');

    $this->addUnmigratedSources(
      array(
        'log',
        'sticky',
        'field_group_sticky',
      )
    );

    $this->addUnmigratedDestinations(
      array(
        'og_group_ref',
        'field_radioactivity',
        'field_logo',
        'field_logo:file_class',
        'field_logo:language',
        'field_logo:destination_dir',
        'field_logo:destination_file',
        'field_logo:file_replace',
        'field_logo:preserve_files',
        'field_logo:source_dir',
        'field_logo:urlencode',
        'field_logo:alt',
        'field_logo:title',
        'field_number_of_attendees',
        'field_offsite_url',
        'field_offsite_url:language',
        'field_organizers',
        'field_registration',
        'field_registration_type',
        'field_topics:create_term',
        'field_topics:ignore_case',
        'field_address:administrative_area',
        'field_address:sub_administrative_area',
        'field_address:locality',
        'field_address:dependent_locality',
        'field_address:postal_code',
        'field_address:premise',
        'field_address:sub_premise',
        'field_address:organisation_name',
        'field_address:name_line',
        'field_address:first_name',
        'field_address:last_name',
        'field_address:data',
        'field_location', // You may want to default this?
      )
    );
  }

  protected function translateFeatured($value) {
    if($value == 'Featured') {
      return 1;
    }
    else {
      return 0;
    }
  }
}
