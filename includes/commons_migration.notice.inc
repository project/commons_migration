<?php

/**
 * @file Notice migration classes for Commons
 */

class CommonsNoticeMigration extends CommonsNodeMigration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);
    $this->description = t('Migration of Commons 2.x "Page" content');

    if ($arguments['destination_type'] == 'post') {
      $this->addNoticeTargetMapptings(); // Default
    } else if ($arguments['destination_type'] == 'notice') {
      $this->addPostTargetMappings();
    }



    //TODO: Set a highwater field

    $this->addUnmigratedSources(
      array(
        'log',
      )
    );
  }


  /**
   * Add the field mappings for the target type 'notice'
   */
  protected function addNoticeTargetMapptings() {
    $this->addFieldMapping('field_topics', 1)
      ->sourceMigration('CommonsTaxonomyTags');
    $this->addFieldMapping('field_topics:source_type')
      ->defaultValue('tid');

    $this->addUnmigratedDestinations(
      array(
        'og_group_ref',
        'field_radioactivity',
        'field_topics:create_term',
        'field_topics:ignore_case',
      )
    );
  }


  /**
   * Add the field mappings for the target type 'post'
   */
  protected function addPostTargetMappings() {

  }
}
