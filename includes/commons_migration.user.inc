<?php


class CommonsUserMigration extends DrupalUser6Migration {
  public function __construct(array $arguments) {

    $this->sourceOptions['track_changes'] = TRUE;

    parent::__construct($arguments);

    $this->addFieldMapping('is_new', NULL, FALSE)->defaultValue(TRUE);
    $this->addFieldMapping('uid', 'uid', FALSE);

    $this->addFieldMapping('field_name_first', 'profile_name');
    $this->addFieldMapping('field_name_first:language', 'language')
      ->description(t('Set to the same language as the user settings'));

    $this->addFieldMapping('field_name_last', 'profile_last_name');
    $this->addFieldMapping('field_name_last:language', 'language')
      ->description(t('Set to the same language as the user settings'));

    $this->addFieldMapping('field_bio', 'profile_aboutme');
    $this->addFieldMapping('field_bio:language', 'language')
      ->description(t('Set to the same language as the user settings'));

    $this->addFieldMapping('field_twitter_url', 'profile_twitter_link');
    $this->addFieldMapping('field_twitter_url:language', 'language')
      ->description(t('Set to the same language as the user settings'));
    $this->addFieldMapping('field_twitter_url:title', 'profile_twitter_link')
      ->description(t('Set the title to the url value'));

    $this->addFieldMapping('field_facebook_url', 'profile_facebook_link');
    $this->addFieldMapping('field_facebook_url:language', 'language')
      ->description(t('Set to the same language as the user settings'));
    $this->addFieldMapping('field_facebook_url:title', 'profile_facebook_link')
      ->description(t('Set the title to the url value'));

    $this->addFieldMapping('field_linkedin_url', 'profile_linkedin_link');
    $this->addFieldMapping('field_linkedin_url:language', 'language')
      ->description(t('Set to the same language as the user settings'));
    $this->addFieldMapping('field_linkedin_url:title', 'profile_linkedin_link')
      ->description(t('Set the title to the url value'));


    $this->addUnmigratedDestinations(
      array(
        'og_user_node',
        'message_subscribe_email',
        'field_facebook_url:attributes',
        'field_linkedin_url:attributes',
        'field_twitter_url:attributes',
      )
    );

  }
}
