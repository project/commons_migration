<?php

/**
 * @file
 * Flag migration classes for Commons
 */

/**
 *
 */
class CommonsFlagMigration extends DrupalMigration {

  public function __construct(array $arguments) {
    parent::__construct($arguments);
    $this->description = t('Migration of Commons 2.x "Flag" content');
    $this->sourceFlag = $arguments['source_flag'];
    $this->destinationFlag = $arguments['destination_flag'];

    $this->source = new MigrateSourceSQL($this->query(), array(), NULL);
    $this->map = new MigrateSQLMap($this->machineName,
      array(
      'fcid' => array('type' => 'int',
                    'unsigned' => TRUE,
                    'not null' => TRUE,
                    'description' => 'Source Flag ID',
                   ),
     ),
     MigrateDestinationFlagSimple::getKeySchema()
    );

    $this->flag = flag_get_flag($this->destinationFlag);
    $this->destination = new MigrateDestinationFlagSimple($this->flag->fid);

    $this->addFieldMapping('fid')
         ->defaultValue($this->flag->fid);
    $this->addFieldMapping('content_type', 'content_type');
    $this->addFieldMapping('content_id', 'content_id')
         ->sourceMigration($arguments['source_migrations']);
    $this->addFieldMapping('uid', 'uid')
         ->sourceMigration('CommonsUser');
    $this->addFieldMapping('timestamp', 'timestamp');
  }

  protected function query() {
    $query = Database::getConnection('default', $this->sourceConnection)
             ->select('flag_content', 'fc')
             ->fields('fc', array('fcid', 'content_type', 'content_id', 'timestamp', 'uid'));
    $query->innerJoin('flags', 'f', 'fc.fid=f.fid');
    $query->condition('f.name', $this->sourceFlag);
    return $query;
  }
}
