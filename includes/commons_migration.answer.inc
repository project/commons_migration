<?php

/**
 * @file
 * Answer migration classes for Commons
 */

/**
 *
 */
class CommonsAnswerMigration extends CommonsNodeMigration {

  public function __construct(array $arguments) {
    parent::__construct($arguments);
    $this->description = t('Migration of Commons 2.x "Answer" content');

    //TODO: Set a highwater field


    $this->addFieldMapping('field_related_question', 'field_answer_question');

    $this->addUnmigratedSources(
      array(
        'log',
      )
    );

    $this->addUnmigratedDestinations(
      array(
        'og_group_ref',
        'field_radioactivity',
      )
    );
  }
}



