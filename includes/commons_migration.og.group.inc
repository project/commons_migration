<?php

/**
 * @file
 * Upgrade nodes that are groups, by settings the correct field value.
 */

class CommonsMigrationOgMigrateGroup extends OgMigrateGroup {

  /**
   * Indicate we are updating existing data.
   */
  protected $systemOfRecord = Migration::SOURCE;

  public function __construct(array $arguments) {

    parent::__construct($arguments);

    //TODO: get the real author
    $this->addFieldMapping('uid')->defaultValue(1);
    $this->removeFieldMapping('nid');
  }
}
