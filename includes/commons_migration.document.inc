<?php

/**
 * @file
 * Document migration classes for Commons
 */

/**
 *
 */
class CommonsDocumentMigration extends CommonsNodeMigration {

  public function __construct(array $arguments) {
    parent::__construct($arguments);
    $this->description = t('Migration of Commons 2.x "Document" content');

    $this->addFieldMapping('sticky', 'field_featured_content');

    $this->addFieldMapping('field_topics', 1)
      ->sourceMigration('CommonsTaxonomyTags');
    $this->addFieldMapping('field_topics:source_type')
      ->defaultValue('tid');
    $this->addUnmigratedSources(
      array(
        'log',
        'field_group_sticky',
        'sticky',
      )
    );

    $this->addUnmigratedDestinations(
      array(
        'og_group_ref',
        'field_radioactivity',
        'field_topics:create_term',
        'field_topics:ignore_case',
        'field_document_file:language',
        'field_document_file:display',

      )
    );
  }

  function prepareRow($row) {

  }
}
