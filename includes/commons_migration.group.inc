<?php

/**
 * @file Event migration classes for Commons
 */

class CommonsGroupMigration extends CommonsNodeMigration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);
    $this->description = t('Migration of Commons 2.x "Group" content');

    $this->addUnmigratedSources(
      array(
        'log',
        'sticky',
        'field_group_sticky',
      )
    );

  }
}
