<?php

/**
 * @file
 * Add per-bundle OG roles.
 *
 * Class should be included only if this is an upgrade from branch 7.x-1.x
 * to branch 7.x-2.x
 */


class OgMigrateRoles extends OgEntityMigration {

  /**
   * Indicate we are updating existing data.
   */
  protected $systemOfRecord = Migration::SOURCE;

}
