<?php

/**
 * @file Page migration classes for Commons
 */

class CommonsPageMigration extends CommonsNodeMigration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);
    $this->description = t('Migration of Commons 2.x "Page" content');



    //TODO: Set a highwater field

    $this->addUnmigratedSources(
      array(
        'log',
      )
    );
  }
}
